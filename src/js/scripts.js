// Current page for the pagination
var current_page = 1;
// Number of records on the page
var records_per_page = 3;

window.onload = function() {

    $('.carousel').carousel({
        interval: 20000,
        pause: "hover"
    });

    // Start the slider with a random image every time the page is refreshed
    let random_slide = Math.floor(Math.random() * 5);
    $(".carousel-item")[random_slide].classList.add("active");

    // highlights
    this.document.getElementById("search-btn").addEventListener("click", function() {
        let text = document.getElementById("search-bar").value;
        // Removes all the highlighted content
        $('body').removeHighlight();
        // Highlights the new content
        $('body').highlight(text);
    });

    // Adds the page numbers in the HTML
    addPageItems();
    // Sets the pagination to the first page
    changePage(current_page);
};

/**
 * Function triggered when 'Read More' is clicked. Shows the entire text of the article. 
 */
function readMore(elem) {
    elem.parentElement.getElementsByClassName("card-text")[0].classList.remove("partial-text");
    elem.disabled = true;
}

/**
 * Function triggered when 'Previous' button is clicked. It changes the page to the previous one.
 */
function prevPage() {
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

/**
 * Function triggered when 'Next' button is clicked. It changes the page to the next one.
 */
function nextPage() {
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}

/**
 * Function triggered when a page is clicked. It changes the page to the one that was clicked.
 */
function pageClick(page) {
    current_page = page;
    changePage(page);
}

/**
 * It changes the page
 */
function changePage(page) {
    // Remove all the active states of the pages
    $(".pagination .page-item").each((_, element) => {
        element.classList.remove("active");
    });

    // Set active state for the selected page
    $(".pagination .page-item")[page].classList.add("active");

    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    // Remove first the articles in the page
    $('.article-container')[0].innerHTML = "";
    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < articlesJson.length; i++) {
        // Add the articles from the next page
        addArticle(articlesJson[i].title, articlesJson[i].text, articlesJson[i].image);
    }

    // Hide or show the next and prev button
    if (page == 1) {
        $("#btn_prev")[0].classList.add("disabled");
    } else {
        $("#btn_prev")[0].classList.remove("disabled");
    }

    if (page == numPages()) {
        $("#btn_next")[0].classList.add("disabled");
    } else {
        $("#btn_next")[0].classList.remove("disabled");
    }
}

/**
 * Returns the total number of pages based on the number of articles
 */
function numPages() {
    return Math.ceil(articlesJson.length / records_per_page);
}

/**
 * It adds the corresponding number of pages in the HTML
 */
function addPageItems() {
    pages_number = numPages();
    if (pages_number > 1) {
        for (let i = 2; i <= pages_number; i++) {
            $("#pages_container")[0].innerHTML += '<li class="page-item" onclick="pageClick(' + i + ' )"><a class="page-link" href="#">' + i + '</a></li>'
        }
    }
}

/**
 * Adds an article with the specific template
 */
function addArticle(title, text, image) {
    $('.article-container')[0].innerHTML = $('.article-container')[0].innerHTML + '<div class="card mb-3" style="max-width:' +
        '90%;"> <div class="row no-gutters"> <div class="col-md-4"> ' +
        '<img class="bd-placeholder-img float-sm-left" width="100%" height="200px" src="./' + image + '"/>' +
        '</div> <div class="col-md-8"> <div class="card-body"> <h5 class="card-title search-content">' + title + '</h5>' +
        '<p class="card-text partial-text search-content">' + text + '</p> <button class="btn btn-link float-right" ' +
        ' data-toggle="collapse" onclick="readMore(this)" aria-expanded="true" aria-controls="collapseExample"> ' +
        'Read more </button> </div> </div> </div> </div>'
}

// Text for the content of the articles
var articlesJson = [{
        "title": "Six-Pack of Peaks Challenge",
        "text": "Participants who register for any official Six-Pack of Peaks Challenge receive route advice and access to an encouraging online community. After signing up, participants strive to bag six designated peaks in the region they choose within a designated calendar span. With part of the proceeds going towards the non-profit Big City Mountaineers, it’s not only the participants that benefit from taking up the challenge.",
        "image": "img/mountain_2.jpg"
    },
    {
        "title": "The 52 Hike Challenge",
        "text": "An inspiring online-based hiking challenge that encourages participants to hike once a week for an entire year, the original 52 Hike Challenge offers free support and access to community logs. Great for that extra motivation needed to get outside, those looking to sign up for the 52 Hike Challenge can also buy in to receive patches and medals upon the challenge completion.",
        "image": "img/mountain_1.jpeg"

    },
    {
        "title": "AMC Four Thousand Footer Club",
        "text": "Also known as the 4K Club, this 60-plus-year-old set of hiking challenges is run by the Appalachian Mountain Club (AMC). The first AMC list to aim towards is The White Mountain Four Thousand Footers. Hikers need to bag all 48 of these New Hampshire peaks to get on the finisher’s roster.",
        "image": "img/mountain_3.jpg"
    },
    {
        "title": "Carolina Mountain Club Waterfall Challenge",
        "text": "The Carolina Mountain Club has come up with a list of 100 waterfalls that are well worth a visit. While it’s hard to quantify what makes any one waterfall “the best”, the list from the Carolina Mountain Club ensures a wide range of adventures if you try to see them all. The colorful “WC100” patch for completing the challenge is a monumental achievement.",
        "image": "img/mountain_1.jpeg"
    },
    {
        "title": "Fire Tower Challenge",
        "text": "Sponsored by the Adirondack Mountain Club, the Fire Tower Challenge entails participants making their way to the top of at least 23 fire tower summits in the Adirondack and Catskill Mountains of New York. Climbing to the top of the actual fire tower is not mandatory.",
        "image": "img/mountain_4.jpg"
    },
    {
        "title": "Tahoe Rim Challenge",
        "text": "Open to hikers, mountain bikers and equestrians, the Tahoe Rim Challenge offers six annual challenge routes. They each explore some of the best trails that Lake Tahoe has to offer. Participants choose between signing up as a Trail-Blazer or Trail-Explorer, with lengths of the challenge routes varying between the two.",
        "image": "img/mountain_3.jpg"
    },
    {
        "title": "365 Mile Challenge",
        "text": "A pay-to-play hiking challenge that provides community support towards the goal of trekking a one-mile hike every day of the year. It doesn’t just have to be hiking to complete the 365 Mile challenge, and any self-propelled mile will do. Different memberships levels for the 365 Mile Challenge include different amounts of swag and gear.",
        "image": "img/mountain_1.jpeg"
    },
    {
        "title": "“Hike the Hoodoos!” Challenge",
        "text": "Within Bryce Canyon National Park in southern Utah, “Hike the Hoodoos!” is a great challenge for kids and those who like to be outside. The task is to trek at least three miles within the park along specified trails. All the while collecting special “Hike the Hoodoos” benchmarks along the way. For a heartier challenge, a total of nine benchmarks can be collected, taking at least 18-miles of hiking to accomplish.",
        "image": "img/mountain_2.jpg"
    },
    {
        "title": "The 52 Hike Challenge",
        "text": "An inspiring online-based hiking challenge that encourages participants to hike once a week for an entire year, the original 52 Hike Challenge offers free support and access to community logs. Great for that extra motivation needed to get outside, those looking to sign up for the 52 Hike Challenge can also buy in to receive patches and medals upon the challenge completion.",
        "image": "img/mountain_5.jpg"
    },
    {
        "title": "AMC Four Thousand Footer Club",
        "text": "Also known as the 4K Club, this 60-plus-year-old set of hiking challenges is run by the Appalachian Mountain Club (AMC). The first AMC list to aim towards is The White Mountain Four Thousand Footers. Hikers need to bag all 48 of these New Hampshire peaks to get on the finisher’s roster.",
        "image": "img/mountain_2.jpg"
    },
    {
        "title": "Carolina Mountain Club Waterfall Challenge",
        "text": "The Carolina Mountain Club has come up with a list of 100 waterfalls that are well worth a visit. While it’s hard to quantify what makes any one waterfall “the best”, the list from the Carolina Mountain Club ensures a wide range of adventures if you try to see them all. The colorful “WC100” patch for completing the challenge is a monumental achievement.",
        "image": "img/mountain_1.jpeg"
    },
    {
        "title": "Carolina Mountain Club Waterfall Challenge",
        "text": "The Carolina Mountain Club has come up with a list of 100 waterfalls that are well worth a visit. While it’s hard to quantify what makes any one waterfall “the best”, the list from the Carolina Mountain Club ensures a wide range of adventures if you try to see them all. The colorful “WC100” patch for completing the challenge is a monumental achievement.",
        "image": "img/mountain_4.jpg"
    },
    {
        "title": "Fire Tower Challenge",
        "text": "Sponsored by the Adirondack Mountain Club, the Fire Tower Challenge entails participants making their way to the top of at least 23 fire tower summits in the Adirondack and Catskill Mountains of New York. Climbing to the top of the actual fire tower is not mandatory.",
        "image": "img/mountain_3.jpg"
    },
    {
        "title": "365 Mile Challenge",
        "text": "A pay-to-play hiking challenge that provides community support towards the goal of trekking a one-mile hike every day of the year. It doesn’t just have to be hiking to complete the 365 Mile challenge, and any self-propelled mile will do. Different memberships levels for the 365 Mile Challenge include different amounts of swag and gear.",
        "image": "img/mountain_1.jpeg"
    },
    {
        "title": "“Hike the Hoodoos!” Challenge",
        "text": "Within Bryce Canyon National Park in southern Utah, “Hike the Hoodoos!” is a great challenge for kids and those who like to be outside. The task is to trek at least three miles within the park along specified trails. All the while collecting special “Hike the Hoodoos” benchmarks along the way. For a heartier challenge, a total of nine benchmarks can be collected, taking at least 18-miles of hiking to accomplish.",
        "image": "img/mountain_5.jpg"
    },
    {
        "title": "The 52 Hike Challenge",
        "text": "An inspiring online-based hiking challenge that encourages participants to hike once a week for an entire year, the original 52 Hike Challenge offers free support and access to community logs. Great for that extra motivation needed to get outside, those looking to sign up for the 52 Hike Challenge can also buy in to receive patches and medals upon the challenge completion.",
        "image": "img/mountain_3.jpg"
    },
    {
        "title": "AMC Four Thousand Footer Club",
        "text": "Also known as the 4K Club, this 60-plus-year-old set of hiking challenges is run by the Appalachian Mountain Club (AMC). The first AMC list to aim towards is The White Mountain Four Thousand Footers. Hikers need to bag all 48 of these New Hampshire peaks to get on the finisher’s roster.",
        "image": "img/mountain_1.jpeg"
    }
];